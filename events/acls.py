from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json


def get_photo(city, state):
    # Use the Pexels API

    # Create a dictionary for the headers to use in the request
    headers = {"Authorization": PEXELS_API_KEY}
    # Create the URL for the request with the city and state
    url = f"https://api.pexels.com/v1/search?query={city}&{state}"
    # Make the request
    response = requests.get(url, headers=headers)
    # Parse the JSON response
    pic = json.loads(response.content)
    # print(pic)
    # Return a dictionary that contains a `picture_url` key and
    #   one of the URLs for one of the pictures in the response
    # print(pic["photos"][0]["src"]["original"])
    return {"picture_url": pic["photos"][0]["src"]["original"]}


def get_weather_data(city, state):
    # Use the Open Weather API

    # Create the URL for the geocoding API with the city and state
    url_cord = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},&limit=&appid={OPEN_WEATHER_API_KEY}"
    # Make the request
    response = requests.get(url_cord)
    # Parse the JSON response
    coordinates = json.loads(response.content)
    # Get the latitude and longitude from the response

    lat = coordinates[0]["lat"]
    lon = coordinates[0]["lon"]
    # Create the URL for the current weather API with the latitude
    #   and longitude
    url_weather = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}"
    # Make the request
    response_weather = requests.get(url_weather)
    # Parse the JSON response
    weather = json.loads(response_weather.content)
    # Get the main temperature and the weather's description and put
    #   them in a dictionary

    description = weather["weather"][0]["description"]
    # convert kelvin to fahrenheit
    fahrenheit = int(float(weather["main"]["temp"]) - 273.15) * 9/5 + 32

    # Return the dictionary
    return {"temp": fahrenheit,
            "description": description}
